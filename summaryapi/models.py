
import datetime

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from django.core.validators import MaxValueValidator, MinValueValidator


class Book(models.Model):
    title = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
    release_year = models.IntegerField()
    # cover = models.ImageField(blank=True, null=True) # Add later

    def __str__(self):
        return self.title


class Summary(models.Model):
    text = models.TextField()
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.text


class Vote(models.Model):
    rating = models.IntegerField(validators=[MinValueValidator(-1), MaxValueValidator(1)])
    summary = models.ForeignKey(Summary, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
