from django.apps import AppConfig


class SummaryapiConfig(AppConfig):
    name = 'summaryapi'
