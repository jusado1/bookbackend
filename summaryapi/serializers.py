from typing import Optional

from django.contrib.auth.models import Group, User
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.db.models.aggregates import Sum
from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from .models import *


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'email', 'is_superuser']
        extra_kwargs = {
            'password': {'write_only': True},
            'is_superuser': {'read_only': True}
        }

    def create(self, validated_data):
        password = validated_data.pop('password')
        user = User(**validated_data)
        user.set_password(password)
        user.save()
        return user


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ['id', 'name']


class BookSerializer(serializers.ModelSerializer):
    # summaries = SummarySerializer(source='summary_set', many=True, read_only=True)
    class Meta:
        model = Book
        fields = ['id', 'title', 'author', 'release_year']  # , 'summaries']


class SummarySerializer(serializers.ModelSerializer):
    author = serializers.CharField(source='user.username', read_only=True)
    # votes = serializers.SerializerMethodField('get_votes')
    votes = serializers.IntegerField(read_only=True)
    vote_status = serializers.SerializerMethodField('get_vote_status')

    class Meta:
        model = Summary
        fields = ['id', 'text', 'book', 'author', 'votes', 'vote_status']
        # extra_kwargs = {
        #     'votes': {'read_only': True}
        # }


    def get_votes(self, summary: Summary):
        votes = summary.vote_set.aggregate(Sum('rating'))
        count = votes['rating__sum'] or 0
        return count

    def get_vote_status(self, summary: Summary):
        status = 0

        user = self.get_user()
        if user is None or user.id is None:
            return status

        try:
            vote = summary.vote_set.filter(user=user.id).get()
            status = vote.rating
        except ObjectDoesNotExist:
            pass
        except MultipleObjectsReturned:
            print('Got more than one vote in serializer')

        return status

    def get_user(self) -> Optional[User]:
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
        return user


class VoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vote
        fields = ['id', 'rating', 'summary']


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)

        # Add extra responses here
        data['user'] = UserSerializer(self.user).data

        return data

    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        # Add custom claims
        token['is_admin'] = user.is_superuser

        return token
