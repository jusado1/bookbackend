from collections import OrderedDict
from django.db.models import base
from django.db.models.functions import Coalesce
from django.db.models.query import QuerySet
from django.http.response import HttpResponse
from rest_framework.exceptions import NotAuthenticated, PermissionDenied
from rest_framework.permissions import AllowAny, IsAuthenticated
from bookbackend.permissions import *
from django.contrib.auth.models import Group, User
from django.http import JsonResponse
from django.shortcuts import render
from rest_framework import permissions, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status
from rest_framework_simplejwt.views import TokenObtainPairView

from .models import *
from .serializers import *


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [(AllowPost & IsNotAuthenticated)
                          | (IsOwner & ReadOnly)]

    def post(self, request, format=None):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [IsAdminUser]


class BookViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows books to be viewed or edited.
    """
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = [ReadOnly | IsAdminUser]

    @action(detail=True)
    def summaries(self, request, pk):
        summaries = (
            Book.objects.get(pk=pk)
            .summary_set
            .annotate(
                votes=Coalesce(
                    Sum('vote__rating'),
                    0
                )
            )
            .order_by('-votes')
        )

        serializer = SummarySerializer(summaries, many=True)
        serializer.context['request'] = request
        return Response(serializer.data)


class SummaryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows summaries to be viewed or edited.
    """
    queryset = Summary.objects.all()
    serializer_class = SummarySerializer
    permission_classes = [IsOwner | ReadOnly | IsAdminUser]

    def perform_create(self, serializer):
        if self.request.user.id is None:
            raise NotAuthenticated
        serializer.save(user=self.request.user)


class VoteViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows votes to be viewed or edited.
    """
    queryset = Vote.objects.all()
    serializer_class = VoteSerializer
    permission_classes = [IsOwner | ReadOnly | IsAdminUser]

    def create(self, request, *args, **kwargs):
        if self.request.user.id is None:
            raise NotAuthenticated

        try:
            vote = Vote.objects.filter(user=request.user, summary=request.data.get('summary')).get()
        except ObjectDoesNotExist:
            vote = None
        except MultipleObjectsReturned:
            print('Got more than one vote in view')
            raise MultipleObjectsReturned

        if vote is None:
            return super().create(request, *args, **kwargs)

        serializer = self.get_serializer(vote, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer


def bad_request(request):
    return JsonResponse(data={"detail": "Bad request."}, safe=False, status=status.HTTP_400_BAD_REQUEST)


def test(request):
    return render(request, 'summaryapi/test.html')
