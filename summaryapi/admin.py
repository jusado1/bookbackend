import django
from django.apps import apps
from django.contrib import admin
from .models import Book, Summary


class SummaryInline(admin.TabularInline):
    model = Summary
    extra = 1


class BookAdmin(admin.ModelAdmin):
    # fieldsets = [
    #     (None,               {'fields': ['title']}),
    #     #('Date information', {'fields': ['pub_date']}),
    # ]
    inlines = [SummaryInline]
    # list_display = ('question_text', 'pub_date', 'was_published_recently')
    # list_filter = ['pub_date']
    # search_fields = ['question_text']


admin.site.register(Book, BookAdmin)


# models = apps.get_models()
# for model in models:
#     try:
#         admin.site.register(model)
#     except admin.sites.AlreadyRegistered:
#         pass
