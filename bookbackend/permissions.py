from django.contrib.auth.models import User
from rest_framework import permissions

class IsOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        # Instance must have an attribute named `user`.
        if request.method in permissions.SAFE_METHODS:
            return True

        print(request.user)
        print(obj.user)

        if hasattr(obj, 'user'):
            return obj.user == request.user
        if isinstance(obj, User):
            return obj == request.user
        return False


class ReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.method in permissions.SAFE_METHODS
    
    def has_object_permission(self, request, view, obj):
        return request.method in permissions.SAFE_METHODS


class AllowPost(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.method == 'POST'


class IsNotAuthenticated(permissions.BasePermission):
    def has_permission(self, request, view):
        return not request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        return not request.user.is_authenticated

class IsAdminUser(permissions.BasePermission):
    def has_permission(self, request, view):
        return bool(request.user and request.user.is_staff)

    def has_object_permission(self, request, view, obj):
        return bool(request.user and request.user.is_staff)